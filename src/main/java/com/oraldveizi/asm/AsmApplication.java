package com.oraldveizi.asm;

import com.oraldveizi.asm.customer.Customer;
import com.oraldveizi.asm.customer.CustomerService;
import com.oraldveizi.asm.user.User;
import com.oraldveizi.asm.user.UserRole;
import com.oraldveizi.asm.user.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class AsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsmApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(UserService userService, CustomerService customerService) {
        return args -> {
            User user1 = new User(1L, "Orald", "Veizi", "orald.veizi@gmail.com", "password", UserRole.TECHNICIAN, "355695560060");
            User user2 = new User(2L, "Juuko", "Uko", "juuko.uko@gmail.com", "password", UserRole.ACCEPTANCE, "355695560060");
            Customer daisy = new Customer(1L, "Daisy", "Tirana", "daisy@tirana.com", "123-45-789", "355684111111");

            userService.saveUser(user1);
            userService.saveUser(user2);
            customerService.save(daisy);
        };
    }

}

package com.oraldveizi.asm.user;

import javax.persistence.Enumerated;


public enum UserRole {
    ACCEPTANCE,
    TECHNICIAN,
    ADMIN
}

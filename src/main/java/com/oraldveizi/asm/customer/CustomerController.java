package com.oraldveizi.asm.customer;

import com.oraldveizi.asm.reapir_sheet.RepairSheetService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/customers")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final RepairSheetService repairSheetService;

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers(@RequestParam(name = "name", required = false) String name, @RequestParam(name = "surname", required = false) String surname) {
        if (name != null && surname != null) {
            return ResponseEntity.ok().body(customerService.findByNameAndSurname(name, surname));
        }

        return ResponseEntity.ok().body(customerService.getCustomers());
    }

    @GetMapping(path = "{customerId}")
    public ResponseEntity<?> getCustomer(@PathVariable("customerId") Long customerId) {
        try {
            return ResponseEntity.ok().body(customerService.getCustomerById(customerId));
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }


    @PostMapping()
    public ResponseEntity<?> registerCustomer(@RequestBody CustomerRequest request) {
        try {
            return ResponseEntity.ok().body(customerService.addNewCustomer(request));
        } catch (IllegalStateException illegalStateException) {
            return ResponseEntity.badRequest().body(illegalStateException.getMessage());
        }
    }

    @PutMapping(path = "{customerId}")
    public ResponseEntity<?> updateCustomer(@PathVariable("customerId") Long customerId, @RequestBody CustomerEmailRequest request) {

        try {
            return ResponseEntity.ok().body(customerService.updateCustomer(customerId, request.getEmail()));

        } catch (IllegalStateException illegalStateException) {
            return ResponseEntity.badRequest().body(illegalStateException.getMessage());
        }

    }

    @DeleteMapping(path = "{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("customerId") Long customerId, @RequestParam(name = "forceDelete", required = false) boolean forceDelete) {

        try {

            Customer customer = customerService.getCustomerById(customerId);

            if (!customer.repairSheets.isEmpty() && !forceDelete) {
                return ResponseEntity.badRequest().body("This customers has repairs sheets records and cannot be deleted!");
            } else if (forceDelete) {
                repairSheetService.deleteAllByCustomerId(customerId);
            }

            customerService.deleteCustomer(customerId);

            return ResponseEntity.accepted().body("Record successfully deleted");
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }
}

package com.oraldveizi.asm.customer;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class CustomerRequest {
    @NotNull
    private final String name;
    @NotNull
    private final String surname;
    @NotNull
    private final String email;

    @NotNull
    private final String phoneNumber;

    private final String fiscalCode;
}

package com.oraldveizi.asm.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    boolean existsByEmail(String email);

    @Query("SELECT c FROM Customer c WHERE c.name LIKE %:name% AND c.surname LIKE %:surname%")
    List<Customer> findByNameAndSurname(String name, String surname);
}

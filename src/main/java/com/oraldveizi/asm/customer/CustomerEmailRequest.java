package com.oraldveizi.asm.customer;

import com.sun.istack.NotNull;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class CustomerEmailRequest {

    @NotNull
    private String email;
}

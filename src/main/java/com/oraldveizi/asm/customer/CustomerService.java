package com.oraldveizi.asm.customer;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final EmailValidator emailValidator;

    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public Customer addNewCustomer(CustomerRequest request) {
        boolean isEmailValid = emailValidator.test(request.getEmail());

        if (!isEmailValid) {
            throw new IllegalStateException("This email is not valid");
        }

        boolean isEmailUnique = customerRepository.existsByEmail(request.getEmail());

        if (isEmailUnique) {
            throw new IllegalStateException("Client with this email already exists!");
        }

        Customer customer = new Customer(
                request.getName(),
                request.getSurname(),
                request.getEmail(),
                request.getFiscalCode(),
                request.getPhoneNumber()
        );

        return customerRepository.saveAndFlush(customer);
    }

    public void deleteCustomer(Long customerId) {
        boolean customerExists = customerRepository.existsById(customerId);

        if (!customerExists) {
            throw new IllegalStateException("Customer with id: " + customerId + " does not exists");
        }

        customerRepository.deleteById(customerId);
    }

    @Transactional
    public Customer updateCustomer(Long customerId, String email) {
        Customer customer = getCustomerById(customerId);

        if (!emailValidator.test(email)) {
            throw new IllegalStateException("This email is not valid");
        }

        if (Objects.equals(email, customer.getEmail())) {
            throw new IllegalStateException("The new email is the same as the old one");
        }

        customer.setEmail(email);

        return customerRepository.saveAndFlush(customer);
    }

    public Customer getCustomerById(Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

        if (optionalCustomer.isEmpty()) {
            throw new IllegalStateException("Customer with id: " + customerId + " does not exists");
        }

        return optionalCustomer.get();
    }

    public List<Customer> findByNameAndSurname(String name, String surname) {
        return customerRepository.findByNameAndSurname(name, surname);
    }

    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    public Customer getCustomer(Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

        if (optionalCustomer.isEmpty()) {
            throw new IllegalStateException("This customer does not exist!");
        }

        return optionalCustomer.get();
    }
}

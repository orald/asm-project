package com.oraldveizi.asm.customer;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.oraldveizi.asm.reapir_sheet.RepairSheet;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "customer")
public class Customer {

    /*
    Attributes
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    private String fullAddress;
    @Column(nullable = false)
    private String phoneNumber;

    @Column(unique = true)
    private String email;

    private String fiscalCode;
    private String pec;

    /*
    Constructors
     */
    public Customer() {
    }

    public Customer(Long id, String name, String surname, String email, String fiscalCode, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.fiscalCode = fiscalCode;
        this.phoneNumber = phoneNumber;
    }

    public Customer(String name, String surname, String email, String fiscalCode, String phoneNumber) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.fiscalCode = fiscalCode;
        this.phoneNumber = phoneNumber;
    }

    /*
 Relationships
  */
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @JsonManagedReference
    public List<RepairSheet> repairSheets = new ArrayList<>();

}

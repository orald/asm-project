package com.oraldveizi.asm.reapir_sheet;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.oraldveizi.asm.customer.Customer;
import com.oraldveizi.asm.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "repair_sheet")
@AllArgsConstructor
@NoArgsConstructor
public class RepairSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serial_device", nullable = false)
    private String serialDevice;

    @Column(name = "problem_description", columnDefinition = "TEXT", nullable = true)
    private String problemDescription;

    @Column(name = "purchase_date")
    private LocalDate purchaseDate;

    @Column(name = "warranty_expiry_date")
    private LocalDate warrantyExpiryDate;

    @Column(name = "completion_date")
    private LocalDate completionDate;
    private String notes;

    @Column(name = "vat_number")
    private Long vatNumber;

    @Column(name = "sdicode")
    private String SDICode;
    private String brand;
    private String template;

    @Enumerated
    @Column(name = "repair_sheet_status")
    private RepairSheetStatus repairSheetStatus;

    @Column(name = "customer_id", nullable = false)
    private Long customerId;

    @Column(name = "acceptance_id", nullable = false)
    private Long acceptanceId;

    @Column(name = "technician_id")
    private Long technicianId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "customer_id_fk"))
    @JsonBackReference
    private Customer customer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "acceptance_id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "acceptance_id_fk"))
    @JsonBackReference
    private User acceptance;

    public RepairSheet(String serialDevice, String problemDescription, LocalDate purchaseDate, LocalDate warrantyExpiryDate, String notes, Long vatNumber, String SDICode, String brand, String template, Long acceptanceId, Long customerId) {
        this.serialDevice = serialDevice;
        this.problemDescription = problemDescription;
        this.purchaseDate = purchaseDate;
        this.warrantyExpiryDate = warrantyExpiryDate;
        this.notes = notes;
        this.vatNumber = vatNumber;
        this.SDICode = SDICode;
        this.brand = brand;
        this.template = template;
        this.acceptanceId = acceptanceId;
        this.customerId = customerId;
        this.repairSheetStatus = RepairSheetStatus.IN_PROCESS;
    }
}

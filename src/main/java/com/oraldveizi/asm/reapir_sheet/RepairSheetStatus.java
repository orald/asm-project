package com.oraldveizi.asm.reapir_sheet;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum RepairSheetStatus {

    COMPLETED("completed"),
    REFUSED("refused"),
    IN_PROCESS("in_process");

    private final String code;

    RepairSheetStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @JsonCreator
    public static RepairSheetStatus getRepairSheetStatusFromCode(String value) {
        for(RepairSheetStatus repairSheetStatus: RepairSheetStatus.values()) {
            if(repairSheetStatus.getCode().equals(value))
                return repairSheetStatus;
        }

        return null;
    }
}

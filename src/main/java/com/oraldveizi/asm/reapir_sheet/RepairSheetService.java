package com.oraldveizi.asm.reapir_sheet;

import com.oraldveizi.asm.customer.Customer;
import com.oraldveizi.asm.customer.CustomerService;
import com.oraldveizi.asm.email.EmailService;
import com.oraldveizi.asm.user.User;
import com.oraldveizi.asm.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RepairSheetService {

    private final RepairSheetRepository repairSheetRepository;
    private final CustomerService customerService;
    private final UserService userService;
    private final EmailService emailService;


    public List<RepairSheet> getRepairSheets() {
        return repairSheetRepository.findAll();
    }

    public RepairSheet addNewRepairSheet(Long customerId, RepairSheetRequest request) {
        Customer customer = customerService.getCustomer(customerId);

        User loggedInUser = userService.getLoggedInUser();

        RepairSheet repairSheet = new RepairSheet(
                request.getSerialDevice(),
                request.getProblemDescription(),
                request.getPurchaseDate(),
                request.getWarrantyExpiryDate(),
                request.getNotes(),
                request.getVatNumber(),
                request.getSDICode(),
                request.getBrand(),
                request.getTemplate(),
                loggedInUser.getId(),
                customer.getId()
        );

        repairSheetRepository.saveAndFlush(repairSheet);

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/repair-sheets/" + repairSheet.getSerialDevice() + "/" + repairSheet.getId()).toUriString());

        String subject = "Your repair service request is accepted!";

        emailService.send(customer.getEmail(), customer.getName(), uri.toString(), subject, subject);

        return repairSheet;

    }

    public void deleteRepairSheet(Long repairSheetId) {
        boolean existsRepairSheet = repairSheetRepository.existsById(repairSheetId);

        if (!existsRepairSheet) {
            throw new IllegalStateException("Repair sheet with id: " + " does not exists");
        }

        repairSheetRepository.deleteById(repairSheetId);
    }

    public RepairSheet findById(Long repairSheetId) {
        Optional<RepairSheet> optionalRepairSheet = repairSheetRepository.findRepairSheetById(repairSheetId);

        if (optionalRepairSheet.isEmpty()) {
            throw new IllegalStateException("Repair sheet does not exist");
        }

        return optionalRepairSheet.get();
    }

    public RepairSheet findBySerialDeviceAndId(String serialDevice, Long repairSheetId) {
        Optional<RepairSheet> optionalRepairSheet = repairSheetRepository.findRepairSheetBySerialDeviceAndId(serialDevice, repairSheetId);

        if (optionalRepairSheet.isEmpty()) {
            throw new IllegalStateException("Repair sheet does not exist");
        }

        return optionalRepairSheet.get();
    }


    public void deleteAllByCustomerId(Long customerId) {
        repairSheetRepository.deleteAllByCustomerId(customerId);
    }

    public RepairSheet updateRepairSheetStatus(Long repairSheetId, RepairSheetStatusRequest request) {
        RepairSheet repairSheet = findRepairSheet(repairSheetId);

        User loggedInUser = userService.getLoggedInUser();

        if (request.getStatus() == null) {
            throw new IllegalStateException("The status inserted is not valid");
        }

        repairSheet.setRepairSheetStatus(request.getStatus());
        repairSheet.setNotes(request.getNotes());
        repairSheet.setCompletionDate(LocalDate.now());
        repairSheet.setTechnicianId(loggedInUser.getId());

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/repair-sheets/" + repairSheet.getSerialDevice() + "/" + repairSheet.getId()).toUriString());

        emailService.send(repairSheet.getCustomer().getEmail(), repairSheet.getCustomer().getName(), uri.toString(), "Your repair service has finished!", repairSheet.getNotes());


        return repairSheetRepository.saveAndFlush(repairSheet);
    }

    public RepairSheet findRepairSheet(Long repairSheetId) {
        Optional<RepairSheet> optionalRepairSheet = repairSheetRepository.findById(repairSheetId);

        if (optionalRepairSheet.isEmpty()) {
            throw new IllegalStateException("No repair sheet found!");
        }

        return optionalRepairSheet.get();
    }

}

package com.oraldveizi.asm.reapir_sheet;

import com.oraldveizi.asm.user.User;
import com.oraldveizi.asm.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/repair-sheets")
@AllArgsConstructor
public class RepairSheetController {

    private final RepairSheetService repairSheetService;

    @GetMapping()
    public ResponseEntity<List<RepairSheet>> getRepairSheets() {
        return ResponseEntity.ok().body(repairSheetService.getRepairSheets());
    }

    @GetMapping(path = "{repairSheetId}")
    public ResponseEntity<?> getRepairSheet(@PathVariable("repairSheetId") Long repairSheetId) {
        try {
            return ResponseEntity.ok().body(repairSheetService.findById(repairSheetId));
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @GetMapping(path = "{serialDevice}/{repairSheetId}")
    public ResponseEntity<?> getRepairSheetForCustomer(@PathVariable("serialDevice") String serialDevice, @PathVariable("repairSheetId") Long repairSheetId) {
        try {
            return ResponseEntity.ok().body(repairSheetService.findBySerialDeviceAndId(serialDevice, repairSheetId));
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PostMapping(path = "/new/{customerId}")
    public ResponseEntity<?> registerRepairSheet(@PathVariable("customerId") Long customerId, @RequestBody RepairSheetRequest request) {
        try {
            return ResponseEntity.ok().body(repairSheetService.addNewRepairSheet(customerId, request));
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PutMapping(path = "{repairSheetId}")
    public ResponseEntity<?> updateRepairSheetStatus(@PathVariable("repairSheetId") Long repairSheetId, @RequestBody RepairSheetStatusRequest request) {

        try {
            return ResponseEntity.ok().body(repairSheetService.updateRepairSheetStatus(repairSheetId, request));
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @DeleteMapping(path = "{repairSheetId}")
    public ResponseEntity<?> deleteRepairSheet(@PathVariable("repairSheetId") Long repairSheetId) {

        try {
            repairSheetService.deleteRepairSheet(repairSheetId);

            return ResponseEntity.accepted().body("Successfully deleted!");
        } catch (IllegalStateException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

}

package com.oraldveizi.asm.reapir_sheet;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RepairSheetRequest {

    @NotNull
    private final String serialDevice;

    @NotNull
    private final String brand;

    private final String template;

    @NotNull
    private final String problemDescription;

    private final LocalDate purchaseDate;
    private final LocalDate warrantyExpiryDate;
    private final String notes;
    private final Long vatNumber;
    private final String SDICode;


}

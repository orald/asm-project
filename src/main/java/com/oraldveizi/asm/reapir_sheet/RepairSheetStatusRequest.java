package com.oraldveizi.asm.reapir_sheet;

import com.sun.istack.NotNull;
import lombok.*;

@Getter
@EqualsAndHashCode
@ToString
public class RepairSheetStatusRequest {


    @NotNull
    private RepairSheetStatus status;

    @NotNull
    private String notes;

    public RepairSheetStatusRequest(RepairSheetStatus status, String notes) {
        this.status = status;
        this.notes = notes;
    }

    public RepairSheetStatusRequest() {
    }
}

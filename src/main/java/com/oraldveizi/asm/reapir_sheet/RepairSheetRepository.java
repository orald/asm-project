package com.oraldveizi.asm.reapir_sheet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface RepairSheetRepository extends JpaRepository<RepairSheet, Long> {

    Optional<RepairSheet> findRepairSheetById(Long repairSheetId);

    Optional<RepairSheet> findRepairSheetBySerialDeviceAndId(String serialDevice, Long repairSheetId);

    @Transactional
    @Modifying
    @Query("DELETE FROM RepairSheet r WHERE r.customerId = ?1")
    void deleteAllByCustomerId(Long customerId);
}

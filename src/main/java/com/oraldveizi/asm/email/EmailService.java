package com.oraldveizi.asm.email;


import com.oraldveizi.asm.customer.Customer;
import com.oraldveizi.asm.reapir_sheet.RepairSheet;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.net.URI;

import static java.awt.SystemColor.text;

@Service
@AllArgsConstructor
public class EmailService implements EmailSender {

    private final JavaMailSender mailSender;

    @Override
    @Async
    public void send(String to, String customerName, String url , String subject, String body) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");


            String email = buildEmail(customerName, url, body);

            helper.setText(email, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom("asmproject@test.com");

            mailSender.send(mimeMessage);
        } catch (MessagingException exception) {
            System.out.println(exception.getMessage());
        }
    }

    //Resource: https://github.com/leemunroe/responsive-html-email-template
    public String buildEmail(String name, String url, String body) {
      return "<!doctype html>\n" +
              "<html>\n" +
              "<head>\n" +
              "    <meta name=\"viewport\" content=\"width=device-width\">\n" +
              "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
              "    <title>Simple Transactional Email</title>\n" +
              "    <style>\n" +
              "        /* -------------------------------------\n" +
              "            INLINED WITH htmlemail.io/inline\n" +
              "        ------------------------------------- */\n" +
              "        /* -------------------------------------\n" +
              "            RESPONSIVE AND MOBILE FRIENDLY STYLES\n" +
              "        ------------------------------------- */\n" +
              "        @media only screen and (max-width: 620px) {\n" +
              "            table[class=body] h1 {\n" +
              "                font-size: 28px !important;\n" +
              "                margin-bottom: 10px !important;\n" +
              "            }\n" +
              "            table[class=body] p,\n" +
              "            table[class=body] ul,\n" +
              "            table[class=body] ol,\n" +
              "            table[class=body] td,\n" +
              "            table[class=body] span,\n" +
              "            table[class=body] a {\n" +
              "                font-size: 16px !important;\n" +
              "            }\n" +
              "            table[class=body] .wrapper,\n" +
              "            table[class=body] .article {\n" +
              "                padding: 10px !important;\n" +
              "            }\n" +
              "            table[class=body] .content {\n" +
              "                padding: 0 !important;\n" +
              "            }\n" +
              "            table[class=body] .container {\n" +
              "                padding: 0 !important;\n" +
              "                width: 100% !important;\n" +
              "            }\n" +
              "            table[class=body] .main {\n" +
              "                border-left-width: 0 !important;\n" +
              "                border-radius: 0 !important;\n" +
              "                border-right-width: 0 !important;\n" +
              "            }\n" +
              "            table[class=body] .btn table {\n" +
              "                width: 100% !important;\n" +
              "            }\n" +
              "            table[class=body] .btn a {\n" +
              "                width: 100% !important;\n" +
              "            }\n" +
              "            table[class=body] .img-responsive {\n" +
              "                height: auto !important;\n" +
              "                max-width: 100% !important;\n" +
              "                width: auto !important;\n" +
              "            }\n" +
              "        }\n" +
              "\n" +
              "        /* -------------------------------------\n" +
              "            PRESERVE THESE STYLES IN THE HEAD\n" +
              "        ------------------------------------- */\n" +
              "        @media all {\n" +
              "            .ExternalClass {\n" +
              "                width: 100%;\n" +
              "            }\n" +
              "            .ExternalClass,\n" +
              "            .ExternalClass p,\n" +
              "            .ExternalClass span,\n" +
              "            .ExternalClass font,\n" +
              "            .ExternalClass td,\n" +
              "            .ExternalClass div {\n" +
              "                line-height: 100%;\n" +
              "            }\n" +
              "            .apple-link a {\n" +
              "                color: inherit !important;\n" +
              "                font-family: inherit !important;\n" +
              "                font-size: inherit !important;\n" +
              "                font-weight: inherit !important;\n" +
              "                line-height: inherit !important;\n" +
              "                text-decoration: none !important;\n" +
              "            }\n" +
              "            #MessageViewBody a {\n" +
              "                color: inherit;\n" +
              "                text-decoration: none;\n" +
              "                font-size: inherit;\n" +
              "                font-family: inherit;\n" +
              "                font-weight: inherit;\n" +
              "                line-height: inherit;\n" +
              "            }\n" +
              "            .btn-primary table td:hover {\n" +
              "                background-color: #34495e !important;\n" +
              "            }\n" +
              "            .btn-primary a:hover {\n" +
              "                background-color: #34495e !important;\n" +
              "                border-color: #34495e !important;\n" +
              "            }\n" +
              "        }\n" +
              "    </style>\n" +
              "</head>\n" +
              "<body class=\"\" style=\"background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\">\n" +
              "<table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;\">\n" +
              "    <tr>\n" +
              "        <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\">&nbsp;</td>\n" +
              "        <td class=\"container\" style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;\">\n" +
              "            <div class=\"content\" style=\"box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;\">\n" +
              "\n" +
              "                <!-- START CENTERED WHITE CONTAINER -->\n" +
              "                <table role=\"presentation\" class=\"main\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;\">\n" +
              "\n" +
              "                    <!-- START MAIN CONTENT AREA -->\n" +
              "                    <tr>\n" +
              "                        <td class=\"wrapper\" style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;\">\n" +
              "                            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\">\n" +
              "                                <tr>\n" +
              "                                    <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\">\n" +
              "                                        <p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">Hi there, " + name + "</p>\n" +
              "                                        <p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">" + body + "</p>\n" +
              "                                        <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;\">\n" +
              "                                            <tbody>\n" +
              "                                            <tr>\n" +
              "                                                <td align=\"left\" style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;\">\n" +
              "                                                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;\">\n" +
              "                                                        <tbody>\n" +
              "                                                        <tr>\n" +
              "                                                            <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;\"> <a href=\"" + url + "\" target=\"_blank\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\">Check status</a> </td>\n" +
              "                                                        </tr>\n" +
              "                                                        </tbody>\n" +
              "                                                    </table>\n" +
              "                                                </td>\n" +
              "                                            </tr>\n" +
              "                                            </tbody>\n" +
              "                                        </table>\n" +
              "                                    </td>\n" +
              "                                </tr>\n" +
              "                            </table>\n" +
              "                        </td>\n" +
              "                    </tr>\n" +
              "\n" +
              "                    <!-- END MAIN CONTENT AREA -->\n" +
              "                </table>\n" +
              "                <!-- END CENTERED WHITE CONTAINER -->\n" +
              "            </div>\n" +
              "        </td>\n" +
              "        <td style=\"font-family: sans-serif; font-size: 14px; vertical-align: top;\">&nbsp;</td>\n" +
              "    </tr>\n" +
              "</table>\n" +
              "</body>\n" +
              "</html>";
    }
}

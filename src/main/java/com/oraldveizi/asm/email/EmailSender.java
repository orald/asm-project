package com.oraldveizi.asm.email;

import com.oraldveizi.asm.customer.Customer;
import com.oraldveizi.asm.reapir_sheet.RepairSheet;

public interface EmailSender {
    void send(String to, String customerName, String url , String subject, String body);
}

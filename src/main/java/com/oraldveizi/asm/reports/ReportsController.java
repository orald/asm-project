package com.oraldveizi.asm.reports;

import com.oraldveizi.asm.reapir_sheet.RepairSheetStatus;
import com.oraldveizi.asm.reapir_sheet.RepairSheetStatusRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "api/reports")
@AllArgsConstructor
public class ReportsController {

    private final ReportService reportService;

    @GetMapping(path = "/periods")
    public ResponseEntity<HashMap<String, Long>> getRepairSheetsOverPeriod(@RequestBody ReportPeriodsRequest request) {
        LocalDate fromDate = request.getFromDate();
        LocalDate toDate;

        toDate = request.getToDate() != null ? request.getToDate() : LocalDate.now();
        Long total = reportService.getReportsOverPeriodsOfTime(fromDate, toDate);

        HashMap<String, Long> map = new HashMap<>();
        map.put("total", total);

        return ResponseEntity.ok().body(map);
    }

    @GetMapping(path = "/status")
    public ResponseEntity<HashMap<String, Long>> getTotalRepairSheetsForStatus(@RequestBody RepairSheetStatusRequest request) {
        RepairSheetStatus status = request.getStatus();

        Long total = reportService.getTotalRepairSheetsForStatus(status);

        HashMap<String, Long> map = new HashMap<>();
        map.put("total", total);

        return ResponseEntity.ok().body(map);
    }

    @GetMapping(path = "/repairs-per-technicians")
    public ResponseEntity<?> getTotalRepairSheetsForEachTechniciansForPeriod(@RequestBody ReportPeriodsRequest request) {
        LocalDate fromDate = request.getFromDate();
        LocalDate toDate;

        toDate = request.getToDate() != null ? request.getToDate() : LocalDate.now();

        List<Object[]> results = reportService.getTotalRepairSheetsForEachTechniciansForPeriod(fromDate, toDate);

        HashMap<String, Long> map = new HashMap<>();

        for(Object[] result: results) {
            map.put((String) result[0], (Long) result[1]);
        }

        return ResponseEntity.ok().body(map);
    }
}

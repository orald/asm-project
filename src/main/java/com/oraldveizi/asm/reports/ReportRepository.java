package com.oraldveizi.asm.reports;

import com.oraldveizi.asm.reapir_sheet.RepairSheet;
import com.oraldveizi.asm.reapir_sheet.RepairSheetStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<RepairSheet, Long> {

    @Query("SELECT COUNT(r) as total_repairs FROM RepairSheet r WHERE r.completionDate between ?1 AND ?2")
    Long getByPeriodsOfTime(LocalDate fromDate, LocalDate toDate);

    @Query("SELECT COUNT(r) as total_repairs FROM RepairSheet r WHERE r.repairSheetStatus = ?1")
    Long getTotalRepairSheetsForStatus(RepairSheetStatus status);

    @Query("SELECT concat(u.firstName, ' ', u.lastName) as full_name, count(rsh.id) as total FROM User u " +
            "join u.repairSheetList rsh WHERE rsh.completionDate between :fromDate AND :toDate" +
            " GROUP BY u.id")
    List<Object[]> getTotalRepairSheetsForEachTechniciansForPeriod(LocalDate fromDate, LocalDate toDate);
}

package com.oraldveizi.asm.reports;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class ReportPeriodsRequest {

    @NotNull
    private final LocalDate fromDate;

    private final LocalDate toDate;

}

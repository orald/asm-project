package com.oraldveizi.asm.reports;

import com.oraldveizi.asm.reapir_sheet.RepairSheetRepository;
import com.oraldveizi.asm.reapir_sheet.RepairSheetStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class ReportService {

    private final ReportRepository reportRepository;
    private final RepairSheetRepository repairSheetRepository;

    public Long getReportsOverPeriodsOfTime(LocalDate fromDate, LocalDate toDate) {
        return reportRepository.getByPeriodsOfTime(fromDate, toDate);
    }

    public Long getTotalRepairSheetsForStatus(RepairSheetStatus status) {
        return reportRepository.getTotalRepairSheetsForStatus(status);
    }

    public List<Object[]> getTotalRepairSheetsForEachTechniciansForPeriod(LocalDate fromDate, LocalDate toDate) {
        return reportRepository.getTotalRepairSheetsForEachTechniciansForPeriod(fromDate, toDate);
    }
}

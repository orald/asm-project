package com.oraldveizi.asm.security.config;
//RESOURCE : https://github.com/getarrays/userservice/blob/main/src/main/java/io/getarrays/userservice/security/SecurityConfig.java

import com.oraldveizi.asm.security.filter.CustomAuthenticationFilter;
import com.oraldveizi.asm.security.filter.CustomAuthorizationFilter;
import com.oraldveizi.asm.user.UserRole;
import com.oraldveizi.asm.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder; //Hmmm

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/login");

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers("/api/login/**", "/api/token/refresh/**").permitAll();
        http.authorizeRequests().antMatchers(GET, "/api/repair-sheets/{serialDevice}/{repairSheetId}").permitAll();
        http.authorizeRequests().antMatchers( "/api/users/**").hasAuthority(UserRole.ADMIN.name());
        http.authorizeRequests().antMatchers("/api/customers/**").hasAuthority(UserRole.ACCEPTANCE.name());
        http.authorizeRequests().antMatchers(GET, "/api/repair-sheets/**").hasAnyAuthority(UserRole.TECHNICIAN.name(), UserRole.ACCEPTANCE.name());
        http.authorizeRequests().antMatchers(POST, "/api/repair-sheets/**").hasAuthority(UserRole.ACCEPTANCE.name());
        http.authorizeRequests().antMatchers(PUT, "/api/repair-sheets/**").hasAuthority(UserRole.TECHNICIAN.name());
        http.authorizeRequests().antMatchers(GET, "/api/reports/**").hasAuthority(UserRole.ADMIN.name());
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter);
        http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setPasswordEncoder(bCryptPasswordEncoder);
        provider.setUserDetailsService(userService);

        return provider;
    }
}
